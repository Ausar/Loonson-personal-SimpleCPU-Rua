`timescale 1ns / 1ps
`include "SimCtrl.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/29 16:59:53
// Design Name: 
// Module Name: SRAM_Ctrl_Unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Bridge(
    input   clk_50M,
    input clk,
    input clr,
    inout [31:0] base_ram_data,  //BaseRAM数据，低8位与CPLD串口控制器共享
    output [19:0] base_ram_addr, //BaseRAM地址
    output [3:0] base_ram_be_n,  //BaseRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output  base_ram_ce_n,       //BaseRAM片选，低有效
    output  base_ram_oe_n,       //BaseRAM读使能，低有效
    output  base_ram_we_n,       //BaseRAM写使能，低有效
    //ExtRAM信号
    inout wire[31:0] ext_ram_data,  //ExtRAM数据
    output wire[19:0] ext_ram_addr, //ExtRAM地址
    output wire[3:0] ext_ram_be_n,  //ExtRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output wire ext_ram_ce_n,       //ExtRAM片选，低有效
    output wire ext_ram_oe_n,       //ExtRAM读使能，低有效
    output wire ext_ram_we_n,       //ExtRAM写使能，低有效
    ///////////////
    input	[31:0] I_sram_addr,
    output	[31:0] I_sram_data,
	output	I_sram_busy,//与M级的访问出现冲突

    input   [31:0]  E_Mem_addr,
    input   [31:0]  E_Mem_write_data,
    output  [31:0]  E_Mem_read_data,
    input	[3:0]   E_MemBitEnable,
	input   	    E_MemReadEnable,
    input           E_MemWriteEnable,
    ////////////////
    output wire txd,  //直连串口发送端
    input  wire rxd,  //直连串口接收端

    output [31:0] UART_DATA_out,
    output [31:0] UART_STATUS_out
    );


    
// #     0x80000000～0x803FFFFF映射到BaseRAM；
// #     0x80400000～0x807FFFFF映射到ExtRAM。
    ////////////////////////////////////////////////////////////////////////////////
    wire [31:0] E_Base_data;

    wire   E_using_Base_Sram ;
    reg     E_using_Base_Sram_r;
    reg     E_using_Base_Sram_r2;
    reg     E_MemWriteEnable_r;
    reg [31:0] E_Mem_write_data_r;

    always @ (posedge clk)begin
        E_using_Base_Sram_r2<=E_using_Base_Sram_r;
        E_MemWriteEnable_r  <=  E_MemWriteEnable;
        E_using_Base_Sram_r <= E_using_Base_Sram; 
        E_Mem_write_data_r  <= E_Mem_write_data;
    end
    wire   [31:0] base_ram_vaddr;

    assign E_using_Base_Sram = ( E_MemReadEnable|E_MemWriteEnable) 
                                                        &&(32'h8000_0000<=E_Mem_addr && E_Mem_addr <=32'h803F_FFFF);

    assign I_sram_busy = E_using_Base_Sram|E_using_Base_Sram_r|E_using_Base_Sram_r2;//Mem级的优先级更高
    assign base_ram_vaddr  = E_using_Base_Sram  ?   E_Mem_addr  :   I_sram_addr;//区分不同的地址
    assign base_ram_addr = base_ram_vaddr[21:2];
    
    assign base_ram_data = (E_MemWriteEnable &E_using_Base_Sram)?   E_Mem_write_data  :  {32{1'bz}};

    // assign base_ram_be_n = ~(E_using_Base_Sram    ?   E_MemBitEnable:4'b1111);//E使用的时候，按照字节使能来进行。否则，全部有效。同时记得要取反。

    reg [3:0] base_ram_be_n_wire ;
    always@(posedge clk) begin
        base_ram_be_n_wire = ~(E_using_Base_Sram    ?   E_MemBitEnable:4'b1111);//E使用的时候，按照字节使能来进行。否则，全部有效。同时记得要取反。
    end
    assign base_ram_be_n =  ~(E_using_Base_Sram    ?   E_MemBitEnable:4'b1111);

    assign base_ram_ce_n = 0;//始终片选中BaseRAM

    assign base_ram_oe_n = (E_MemWriteEnable&E_using_Base_Sram);//|(E_MemWriteEnable_r&E_using_Base_Sram_r);//Mem在写的时候禁止读,否则一直读
    assign base_ram_we_n = ~(E_MemWriteEnable&E_using_Base_Sram);//判断是否在写

    `ifdef BASE_RAM_ECHO
        always @ (posedge clk) 
            if(~base_ram_we_n)
                $display("Writing %x to Base:%x vaddr:%x",E_Mem_write_data,base_ram_addr,E_Mem_addr);
    `endif
    //////////////////////////////////////////////////////////////////////////////////
    
    wire   E_using_Ext_Sram = ( E_MemReadEnable|E_MemWriteEnable) 
                                                        &&(32'h8040_0000<=E_Mem_addr && E_Mem_addr <=32'h807F_FFFF);
    wire    [31:0] ext_ram_vaddr = E_Mem_addr;//默认情况下用0
    assign   ext_ram_addr  = ext_ram_vaddr[21:2];

    assign ext_ram_data = E_MemWriteEnable ?   E_Mem_write_data    :   {32{1'bz}};

    reg [3:0] E_MemBitEnable_r;
    always @ (posedge clk)
        E_MemBitEnable_r <= E_MemBitEnable;

    assign ext_ram_be_n =  ~E_MemBitEnable;//只有E才会用

    assign ext_ram_ce_n = 0;//始终片选中ExtRAM

    assign ext_ram_oe_n = E_MemWriteEnable&E_using_Ext_Sram;//Mem在写的时候禁止读,否则一直读
    assign ext_ram_we_n = ~(E_MemWriteEnable&E_using_Ext_Sram);//判断是否在写

    `ifdef EXT_RAM_ECHO
    always @ (posedge clk) begin
        if(~ext_ram_we_n)
            $display("Writing %x to Ext:%x",E_Mem_write_data,ext_ram_addr);
    end
    `endif

    //////////////////////////////////////////////////////////////////////////////////

    // 0xBFD003F8 数据收发接口
    //0xBFD003FC [1]:recived [0]:IDLE  
    parameter UART_DATA_ADDR = 32'hBFD003F8;
    parameter UART_STATUS_ADDR = 32'hBFD003FC;

    wire [31:0] UART_DATA;
    wire [31:0] UART_STATUS;

    wire RxD_data_ready;
    wire RxD_clear;
    wire [7:0] RxD_data;

    wire TxD_busy;
    wire TxD_start;
    wire [7:0] TxD_data;

    wire RXD_FIFO_wr_en;
    wire RXD_FIFO_full;
    wire [7:0] RXD_FIFO_din;
    wire RXD_FIFO_rd_en;
    wire RXD_FIFO_empty;
    wire [7:0] RXD_FIFO_dout;

    wire TXD_FIFO_wr_en;
    wire TXD_FIFO_full;
    wire [7:0] TXD_FIFO_din;
    wire TXD_FIFO_rd_en;
    wire TXD_FIFO_empty;
    wire [7:0] TXD_FIFO_dout;

    assign UART_DATA = {24'b0,RXD_FIFO_dout};
    assign UART_STATUS = {30'b0,~RXD_FIFO_empty,~TXD_FIFO_full};

    assign RxD_clear = (RxD_data_ready&&(~RXD_FIFO_full))||clr;//收到数据，且FIFO没满，就能继续装
     
    assign TxD_start = (~TxD_busy)&&(~TXD_FIFO_empty);//FIFO不空，且不忙的时候就发送
    assign TxD_data = TXD_FIFO_dout;//来自FIFO的数据

    assign RXD_FIFO_wr_en = RxD_clear;//清空的同时写入
    assign RXD_FIFO_din = RxD_data;//接收端的数据
    assign RXD_FIFO_rd_en = (E_Mem_addr==UART_DATA_ADDR) && (E_MemBitEnable[0]&&E_MemReadEnable);
    
    assign TXD_FIFO_wr_en = (E_Mem_addr==UART_DATA_ADDR) && (E_MemBitEnable[0]&&E_MemWriteEnable);
    assign TXD_FIFO_din = E_Mem_write_data[7:0];
    assign TXD_FIFO_rd_en = TxD_start;//进行发送

    async_receiver #(.ClkFrequency(`ClkFrequency),.Baud(`Baud)) //接收模块，9600无检验位
    uart_r(
        .clk(clk),                       //外部时钟信号
        .RxD(rxd),                           //外部串行信号输入
        .RxD_data_ready(RxD_data_ready),  //数据接收到标志
        .RxD_clear(RxD_clear),       //清除接收标志
        .RxD_data(RxD_data)             //接收到的一字节数据
    );
    async_transmitter #(.ClkFrequency(`ClkFrequency),.Baud(`Baud)) //发送模块，9600无检验位
    uart_t(
        .clk(clk),                  //外部时钟信号
        .TxD(txd),                      //串行信号输出
        .TxD_busy(TxD_busy),       //发送器忙状态指示
        .TxD_start(TxD_start),    //开始发送信号
        .TxD_data(TxD_data)        //待发送的数据
    );


    fifo RXD_FIFO (
        .rst(clr),                  // input wire rst
        .clk(clk),            // input wire wr_clk
        .wr_en(RXD_FIFO_wr_en),              // input wire wr_en
        .din(RXD_FIFO_din),                  // input wire [7 : 0] din
        .full(RXD_FIFO_full),                // output wire full

        .rd_en(RXD_FIFO_rd_en),              // input wire rd_en
        .dout(RXD_FIFO_dout),                // output wire [7 : 0] dout
        .empty(RXD_FIFO_empty)              // output wire empty
        
        );
        fifo TXD_FIFO (
        .rst(clr),                  // input wire rst
        .clk(clk),            // input wire wr_clk
        .wr_en(TXD_FIFO_wr_en),              // input wire wr_en
        .din(TXD_FIFO_din),                  // input wire [7 : 0] din
        .full(TXD_FIFO_full),                // output wire full

        .rd_en(TXD_FIFO_rd_en),              // input wire rd_en
        .dout(TXD_FIFO_dout),                // output wire [7 : 0] dout
        .empty(TXD_FIFO_empty)              // output wire empty
        
        );
    //////////////////////////////////////////////////////////////////////////////////
    //分发不同的数据
    assign I_sram_data = base_ram_data;
    wire [31:0] E_Mem_read_data_raw  =    E_Mem_addr == UART_DATA_ADDR   ?   UART_DATA://UART要用打一拍的地址
                                                                E_Mem_addr == UART_STATUS_ADDR? UART_STATUS:
                                                                (32'h8040_0000<=E_Mem_addr && E_Mem_addr <=32'h807F_FFFF)    ?   ext_ram_data:
                                                                base_ram_data;
    assign  E_Mem_read_data = {{8{E_MemBitEnable[3]}},{8{E_MemBitEnable[2]}},{8{E_MemBitEnable[1]}},{8{E_MemBitEnable[0]}}} & E_Mem_read_data_raw;

    assign UART_DATA_out = UART_DATA;
    assign UART_STATUS_out = UART_STATUS;

endmodule
