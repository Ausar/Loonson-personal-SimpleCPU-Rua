`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:27:14 07/07/2019 
// Design Name: 
// Module Name:    XALU_Test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module XALU_Test(
	input	Clk,
    input signed [31:0] A,
    input signed [31:0] B,
    output reg [63:0] C
    );
	reg signed [63:0]	buf0,buf1,buf2,buf3,buf4,buf5,buf6,buf7,
				buf10,buf11,buf12,buf13,
				buf20,buf21;
	wire oneNeg = A[31]^B[31];
	wire signed [31:0] A_X =	A[31]&oneNeg 	?	B:
								A[31]			?	-A:A;
	wire signed [31:0] B_X =	A[31]&oneNeg 	?	A:
								B[31]			?	-B:B;
	initial begin
		C <= 0;
		{buf0,buf1,buf2,buf3,buf4,buf5,buf6,buf7,
				buf10,buf11,buf12,buf13,
				buf20,buf21}<=0;
	end
	integer i;
	
	always @ (posedge Clk) begin
		buf0 <= A_X[0]?B_X<<0:0;
		buf1 <= A_X[1]?B_X<<1:0;
		buf2 <= A_X[2]?B_X<<2:0;
		buf3 <= A_X[3]?B_X<<3:0;
		buf4 <= A_X[4]?B_X<<4:0;
		buf5 <= A_X[5]?B_X<<5:0;
		buf6 <= A_X[6]?B_X<<6:0;
		buf7 <= A_X[7]?B_X<<7:0;
		
		buf10 <= buf0+buf1;
		buf11 <= buf2+buf3;
		buf12 <= buf4+buf5;
		buf13 <= buf6+buf7;
		
		buf20 <= buf10+buf11;
		buf21 <= buf12+buf13;
		
		C <= buf20+buf21;
	end
	/*always @ (posedge Clk) begin
		C = 0;
		for(i = 0;i<32;i=i+1) begin
			if(A[i]==1'b1)
				C = C + (B<<i);
		end
	end*/
endmodule
