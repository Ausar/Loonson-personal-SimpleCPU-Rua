`timescale 1ns / 1ps
`include "InstrHeader.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 11:16:09
// Design Name: 
// Module Name: ExecuteSdate
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ExecuteSdate(
    input   Clk,
    input   Clr,
	input	[31:0]	D_PC,
	input	[31:0]	D_Instr,
    input   [4:0]   D_RsID,
    input   [4:0]   D_RtID,
    input   [31:0]  D_RsData,
    input 	[31:0]  D_RtData,
    input   [4:0]   D_Shamt,
    input   [15:0]  D_Imm16,
    input	[`InstrBusWidth]  D_InstrBus,
    input 	[3:0]   D_T,
    input			D_WriteRegEnable,
    input	[4:0]   D_RegId,
    
    input           M_WriteRegEnable,
    input   [4:0]   M_RegId,
    input   [31:0]  M_Data,

	(*mark_debug = "true"*)output reg	[31:0]	E_PC,
	(*mark_debug = "true"*)output reg 	[31:0]	E_Instr,
    (*mark_debug = "true"*)output reg   [31:0]   E_WriteMemData,
    output reg [4:0]    E_RtID,

    output reg	[3:0]   E_T,
    output reg			E_WriteRegEnable,
    output reg	[4:0]	E_RegId,
    output reg	[31:0]  E_Data,

    
    output reg	[3:0]   E_MemBitEnable,
	output reg	E_MemReadEnable,
    output reg	E_MemWriteEnable,
	output reg	[4:0]   E_ExtType,

	output	E_XALU_Busy,
	input [31:0] I_next_pc_pass,

	output reg E_Reading_base_n,
	output reg E_Writing_base_n,
	output reg E_Using_base,
	output reg [3:0]  E_base_be_n,
	output reg [31:0] E_base_addr,

	output reg E_Reading_ext_n,
	output reg E_Writing_ext_n,
	output reg E_Using_ext,
	output reg [3:0]  E_ext_be_n,
	output reg [31:0] E_ext_addr,

	output reg E_Reading_UART_Data,
	output reg E_Writing_UART_Data,
	output reg E_Reading_UART_STATUS,

	output reg E_M_FM_singal,
	output I_sram_busy,
	output wire [4:0] E_CNT_Pass,
	output reg E_slti_reg
    );
	wire	`InstrBusWireSet;
	assign	{`InstrBusWireSet}	=	D_InstrBus;

	always @ (posedge Clk) begin
		E_slti_reg <= slti;
	end
    /////////////////////转发////////////////
    wire    [31:0]  MF_Rs = (D_RsID!=0 && D_RsID==E_RegId && E_T==0 && E_WriteRegEnable)		?   E_Data:
                            (D_RsID!=0 && M_RegId==D_RsID && M_WriteRegEnable )					?   M_Data:
                                                                                                    D_RsData;
    wire    [31:0]  MF_Rt = (D_RtID!=0 && D_RtID==E_RegId && E_T==0 && E_WriteRegEnable)		?	E_Data:
                            (D_RtID!=0 && M_RegId==D_RtID && M_WriteRegEnable)					?   M_Data://M级结束（实际上是W级了），那么T必定�
                                                                                                    D_RtData;

    wire    [31:0]  C_Inter;
    	/////////////////////For Mem Addr/////
	wire [31:0] ext_imm32 = {{16{D_Imm16[15]}},D_Imm16};
	wire [31:0] MemAddr = C_Inter;//ext_imm32+MF_Rs;
    wire    [4:0]   ExtType_Inter;
	wire	[3:0]	MemBitEnable_Inter;
    wire			MemWriteEnable_Inter;
    wire    		MemReadEnable_Inter;
	wire MemAddr_in_base_inter = (32'h8000_0000<=MemAddr && MemAddr <=32'h803F_FFFF);
	wire MemAddr_in_ext_inter  = (32'h8040_0000<=MemAddr && MemAddr <=32'h807F_FFFF);

	wire Reading_base_inter = MemAddr_in_base_inter&MemReadEnable_Inter;
	wire Writing_base_inter = MemAddr_in_base_inter&MemWriteEnable_Inter;
	wire Using_base_inter = (MemAddr_in_base_inter)&(MemReadEnable_Inter|MemWriteEnable_Inter);
	wire [3:0] Base_bit_enable_inter = Using_base_inter ? MemBitEnable_Inter:4'b1111;
	wire [31:0] Base_addr_inter = Using_base_inter?MemAddr:I_next_pc_pass;

	wire Reading_ext_inter = MemAddr_in_ext_inter&MemReadEnable_Inter;
	wire Writing_ext_inter = MemAddr_in_ext_inter&MemWriteEnable_Inter;
	wire Using_ext_inter = (MemAddr_in_ext_inter)&(MemReadEnable_Inter|MemWriteEnable_Inter);
	wire [3:0] Ext_bit_enable_inter = Using_ext_inter ? MemBitEnable_Inter:4'b1111;
	wire [31:0] Ext_addr_inter = MemAddr;
	// wire [31:0] Ext_addr_inter = Using_ext_inter?MemAddr:I_next_pc_pass;
	// wire [3:0] Ext_bit_enable_inter = MemBitEnable_Inter;
	// wire [3:0] Ext_bit_enable_inter = (Using_ext_inter?MemBitEnable_Inter:4'b1111);

	wire Reading_UART_Data_inter = MemAddr==32'hBFD003F8&MemReadEnable_Inter;
	wire Writing_UART_Data_inter = MemAddr==32'hBFD003F8&MemWriteEnable_Inter;
	wire Reading_UART_STATUS_inter = MemAddr==32'hBFD003FC&MemReadEnable_Inter;

	// always @ (posedge Clk) begin
	// 	if(Clr)begin
	// 		I_sram_busy<=0;
	// 	end
	// 	else begin
	// 		I_sram_busy<=Using_base_inter;//|E_Using_base;
	// 	end
	// end
	//////////////////////////////////////////////////////////////////////////////////
    ALU ALU(.A(MF_Rs),.B(MF_Rt),.shamt(D_Shamt),.Imm16(D_Imm16),.InstrBus(D_InstrBus),.C(C_Inter),.PC(D_PC));

	/*module Counter(
    input clk,
    input [31:0] data_to_cnt,
    output reg [4:0] cnt_data
    );*/
	Counter Counter(.clk(Clk),.data_to_cnt(MF_Rs),.cnt_data(E_CNT_Pass));
	assign  I_sram_busy = E_Using_base;
	wire [31:0] XALU_HI,
				XALU_LO;
	wire		XALU_Busy_Inter;
	
	// XALU XALU(.Clk(Clk),.Clr(Clr),.InstrBus(D_InstrBus),.XALU_A(MF_Rs),.XALU_B(MF_Rt),.XALU_HI(XALU_HI),.XALU_LO(XALU_LO),.XALU_Busy(XALU_Busy_Inter));




	wire	[1:0]	Offset = MemAddr[1:0];
    MemCtrlUnit MemCtrlUnit(.InstrBus(D_InstrBus),
							.Offset(Offset),
							.ExtType(ExtType_Inter),
							.MemBitEnable(MemBitEnable_Inter),
							.MemReadEnable(MemReadEnable_Inter),
							.MemWriteEnable(MemWriteEnable_Inter));
    //////////////////////////////////////////////////////////////////////////////////////////
    wire    [3:0] E_T_Inter = (D_T > 0)?D_T-1:0;
	wire	[31:0]	Data_Inter = 	mfhi	?	XALU_HI:
									mflo	?	XALU_LO:
												C_Inter;

/*
	output reg E_Reading_base_n,
	output reg E_Writing_base_n,
	output reg E_Using_base,
	output reg [3:0]  E_base_be_n,
	output reg [31:0] E_base_addr,

	output reg E_Reading_ext_n,
	output reg E_Writing_ext_n,
	output reg E_Using_ext,
	output reg [3:0]  E_ext_be_n,
	output reg [31:0] E_ext_addr,

	output reg E_Reading_UART_Data,
	output reg E_Writing_UART_Data,
	output reg E_Reading_UART_STATUS,

	output reg I_SRAM_Busy
*/

    always @ (posedge Clk) begin
        if(Clr) begin
			E_PC <= 0;
			E_Instr<=0;
			E_WriteMemData <= 0;
			E_RtID <= 0;
			E_T <= 0;
			E_WriteRegEnable <= 0;
			E_RegId <= 0;
			E_Data <= 0;
			E_ExtType <= 0;
			E_MemReadEnable <= 0;
			E_MemWriteEnable <= 0;
			E_MemBitEnable <= 0;

			//初始化可能有问题
			E_Reading_base_n<=0;
			E_Writing_base_n<=1;
			E_Using_base<=0;
			E_base_be_n<=0;
			E_base_addr<=0;

			E_Reading_ext_n<=1;
			E_Writing_ext_n<=1;
			E_Using_ext<=0;
			E_ext_be_n<=0;
			E_ext_addr<=0;

			E_Reading_UART_Data<=0;
			E_Writing_UART_Data<=0;
			E_Reading_UART_STATUS<=0;
			E_M_FM_singal<=0;
		end
		else begin
			E_PC <= D_PC;
			E_Instr<=D_Instr;
			E_WriteMemData <= MF_Rt;//<<{Offset,3'b0};
			E_RtID <= D_RtID;
			E_T <= D_T ==4'b0 ? 4'b0 : D_T-1;
			E_WriteRegEnable <= D_WriteRegEnable;
			E_RegId <= D_RegId;
			E_Data <= Data_Inter;
			E_ExtType <= ExtType_Inter;
			E_MemReadEnable <= MemReadEnable_Inter;
			E_MemWriteEnable <= MemWriteEnable_Inter;
			E_MemBitEnable <= MemBitEnable_Inter;


			E_Reading_ext_n<=~Reading_ext_inter;
			E_Writing_ext_n<=~Writing_ext_inter;
			E_Using_ext<=Using_ext_inter;
			E_ext_be_n<=~Ext_bit_enable_inter;
			E_ext_addr<=Ext_addr_inter;


			E_Reading_base_n<=~Reading_base_inter;
			E_Writing_base_n<=~Writing_base_inter;
			E_Using_base<=Using_base_inter;
			E_base_be_n<=~Base_bit_enable_inter;
			E_base_addr<=Base_addr_inter;

			E_Reading_UART_STATUS<=Reading_UART_STATUS_inter;

			E_Reading_UART_Data<=Reading_UART_Data_inter;
			E_Writing_UART_Data<=Writing_UART_Data_inter;
			E_M_FM_singal<=E_WriteRegEnable&E_RegId==D_RtID;
		end
		
    end
		assign E_XALU_Busy = XALU_Busy_Inter;

endmodule

module Counter(
    input clk,
    input [31:0] data_to_cnt,
    output reg [4:0] cnt_data
    );

    wire [4:0] part_data[31:0];
    genvar i;
    generate
        for ( i = 0 ;i<32; i=i+1 ) begin:genpart
            assign part_data[i] = data_to_cnt[i];
        end
    endgenerate
    
    wire [4:0] sum_layer_1[15:0];
    wire [4:0] sum_layer_2[7:0];
    reg [4:0] sum_layer_2_reg[7:0];
    generate 
        for (i=0;i<16;i=i+1)begin:gen_layer_1
            assign  sum_layer_1[i] = part_data[i]+part_data[i+16];
        end
        for (i=0;i<8;i=i+1)begin:gen_layer_2
            assign  sum_layer_2[i] = sum_layer_1[i]+sum_layer_1[i+8];
            always @ (posedge clk) begin
                sum_layer_2_reg[i]<=sum_layer_2[i]; //两级流水
            end
        end
    endgenerate
    integer j;
    always @ (*) begin
       cnt_data = 0;
       for(j=0;j<8;j=j+1)
            cnt_data = cnt_data+sum_layer_2_reg[j]; 
    end
endmodule
