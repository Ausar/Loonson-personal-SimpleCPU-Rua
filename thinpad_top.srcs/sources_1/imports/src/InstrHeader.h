`define InstrBusWidth 54:0
`define InstrBusWireSet lb,lbu,lh,lhu,lw,sb,sh,sw,\
                        addi,addiu,add,addu,sub,subu,\
                        lui,ori,And,Or,Xor,Nor,Andi,Xori,\
                        beq,bne,blez,bgtz,bltz,bgez,\
                        j,jal,jr,jalr,nop,\
                        mult,multu,div,divu,\
                        mfhi,mthi,mflo,mtlo,\
                        sll,srl,sra,sllv,srlv,srav,\
                        slt,slti,sltu,sltiu,\
                        mfc0,mtc0,eret,mul
