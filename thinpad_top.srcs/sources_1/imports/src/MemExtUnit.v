`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 15:04:19
// Design Name: 
// Module Name: MemExtUnit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemExtUnit(
    input   [31:0]  RawMemData,
    input   [1:0]   Offset,
    input   [4:0]   ExtType,
    output  [31:0]  ExtMemData
    );
    wire lb,lbu,lh,lhu,lw;
    assign {lb,lbu,lh,lhu,lw}=ExtType;
    
    wire [31:0] OffsetedData =  RawMemData>>({Offset,3'b0});
	/*Offset==4'd1 ? {8'd0,RawMemData[23:0]}:
                                Offset==4'd2 ? {16'd0,RawMemData[15:0]}:
                                Offset==4'd3 ? {24'd0,RawMemData[7:0]}:
                                               RawMemData;*/
    assign ExtMemData = lhu ?   {16'b0,OffsetedData[15:0]}:
                        lh  ?   {{16{OffsetedData[15]}},OffsetedData[15:0]}:
                        lbu ?   {24'b0,OffsetedData[7:0]}:
                        lb  ?   {{24{OffsetedData[7]}},OffsetedData[7:0]}:
                                OffsetedData;
endmodule
