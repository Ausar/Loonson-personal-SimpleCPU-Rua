`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 15:52:50
// Design Name: 
// Module Name: MemState
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//注意，这一个模块里面实际上把WriteBack级给包括进来�//在后续扩展中，很可能会在这个模块中加入内部流水级,也就是说，很可能这个模块是二级流�
module MemState(
        input   Clk,
        input   Clr,
		input	[31:0]	E_PC,
        input   [31:0]  E_Instr,
        input   [31:0]  E_MemWriteData,
        input   [4:0]   E_RtID,
        input   [31:0]  E_Data,
        input           E_WriteRegEnable,
        input   [4:0]   E_RegId,
        input   [31:0]  E_Mem_read_data,
        // input	[3:0]   E_MemBitEnable,
        input	E_MemReadEnable,
        // input	E_MemWriteEnable,
        input	[4:0]   E_ExtType,
        output  [31:0]  E_MemWriteData_MFed,
        output reg			M_WriteRegEnable,
        output reg	[4:0]   M_RegId,
        output 		[31:0]  M_Data,
		output reg	[31:0]	M_PC,
        // /////////////////////////新加入的SRAM 接口
        input wire E_Reading_base_n,
        input wire E_Reading_ext_n,
        input wire E_Reading_UART_Data,
        input wire E_Writing_UART_Data,
        input wire E_Reading_UART_STATUS,

        input wire [31:0] base_data,
        input wire [31:0] ext_data,
        output wire txd,  //直连串口发送端
        input  wire rxd,  //直连串口接收端
        input wire E_M_FM_singal,
        input wire [4:0] E_CNT_Pass,
        input E_slti_reg
    );

    wire    [31:0]  MF_Rt = E_M_FM_singal				?   M_Data:
															E_MemWriteData;
    wire [31:0] MemReadData_Inter = E_Mem_read_data;
    wire [31:0] Data_to_next_state = E_MemReadEnable ? MemReadData_Inter:E_Data;
    wire [1:0]  Offset_Inter = E_Data[1:0];
    assign  E_MemWriteData_MFed = MF_Rt<<{Offset_Inter,3'b0};//有bug，但是先不修


    
    wire          TxD_valid = E_Writing_UART_Data;
    wire          TxD_ready;
    wire   [7:0]  TxD_din = E_MemWriteData_MFed[7:0];

    wire          RxD_ready = E_Reading_UART_Data;
    wire          RxD_valid;
    wire  [7:0]   RxD_dout;

    wire [31:0] UART_DATA = {24'b0,RxD_dout};
    wire [31:0] UART_STATUS = {30'b0,RxD_valid,TxD_ready};
    UART_Unit UART_Unit(
        .clk(Clk),
        .clr(Clr),
        .rxd(rxd),
        .txd(txd),

        .TxD_valid(TxD_valid),
        .TxD_ready(TxD_ready),
        .TxD_din(TxD_din),

        .RxD_ready(RxD_ready),
        .RxD_valid(RxD_valid),
        .RxD_dout(RxD_dout)
    );




    reg [1:0] M_Offset;
    reg [4:0] M_ExtType;
    reg [31:0] M_ALU_Data;
    reg [31:0] M_Base_Data;
    reg [31:0] M_Ext_Data;
    reg [31:0] M_UART_DATA;
    reg [31:0] M_UART_STATUS;
	reg		M_MemReadEnable;
    reg [31:0] M_RawData;

    reg M_Reading_base;
    reg M_Reading_ext;
    reg M_reading_UART_DATA;
    reg M_reading_UART_STATUS;
    always @ (posedge Clk) begin
		if(Clr) begin
			M_WriteRegEnable <= 0;
			M_RegId <= 0;
			M_Offset <= 0;
			M_ExtType <= 0;
			M_MemReadEnable <= 0;
			M_PC <= 0;
            M_ALU_Data <= 0;
            M_Base_Data<=0;
            M_Ext_Data <=0;
            M_UART_DATA<=0;
            M_UART_STATUS<=0;

            M_Reading_base<=0;
            M_Reading_ext<=0;
            M_reading_UART_DATA<=0;
            M_reading_UART_STATUS<=0;
		end
		else begin
			M_WriteRegEnable <= E_WriteRegEnable;
			M_RegId <= E_RegId;
			M_Offset <= Offset_Inter;
			M_ExtType <= E_ExtType;
			M_MemReadEnable <= E_MemReadEnable;
			M_PC <= E_PC;
            M_ALU_Data <=E_slti_reg?E_CNT_Pass:E_Data;
            M_Base_Data<=base_data;
            M_Ext_Data <=ext_data;
            M_UART_DATA<=UART_DATA;
            M_UART_STATUS<=UART_STATUS;

            M_Reading_base<=~E_Reading_base_n;
            M_Reading_ext<=~E_Reading_ext_n;
            M_reading_UART_DATA<=E_Reading_UART_Data;
            M_reading_UART_STATUS<=E_Reading_UART_STATUS;
            M_RawData = ~E_Reading_ext_n    ?   ext_data:
                        ~E_Reading_base_n   ?   base_data:
                        E_Reading_UART_Data ?   UART_DATA:
                                                UART_STATUS;

		end
    end
    //////////////////////////////////////

    // wire [31:0] M_RawData = M_Reading_base          ?   M_Base_Data:
    //                         M_Reading_ext           ?   M_Ext_Data:
    //                         M_reading_UART_DATA     ?   M_UART_DATA:
    //                                                     M_UART_STATUS;
                            //M_reading_UART_STATUS   ?   M_UART_STATUS:M_Ext_Data;
	wire [31:0] ExtMemData;
    MemExtUnit MemExtUnit(  .RawMemData(M_RawData),
                            .Offset(M_Offset),
                            .ExtType(M_ExtType),
                            .ExtMemData(ExtMemData));
    assign M_Data = M_MemReadEnable ? ExtMemData:M_ALU_Data;
endmodule
