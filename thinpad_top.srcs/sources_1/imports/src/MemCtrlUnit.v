`timescale 1ns / 1ps
`include "InstrHeader.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 14:31:46
// Design Name: 
// Module Name: MemCtrlUnit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemCtrlUnit(
    input   [`InstrBusWidth]  InstrBus,
    input   [1:0]   Offset,//地址末尾的两位
	output  [4:0]   ExtType,
    output  [3:0]   MemBitEnable,
    output  MemReadEnable,
    output  MemWriteEnable
    );

    wire    `InstrBusWireSet;
	assign	{`InstrBusWireSet}	=	InstrBus;
    assign	ExtType = {lb,lbu,lh,lhu,lw};
    assign  MemWriteEnable = sb|sh|sw;
    assign  MemReadEnable = lb|lbu|lh|lhu|lw;

    assign  MemBitEnable  = sw|lw       ?   4'b1111:
                            sh|lh|lhu   ?   4'b0011<<Offset:
                            sb|lb|lbu   ?   4'b0001<<Offset:
                                            4'b0000;
    
    
endmodule
