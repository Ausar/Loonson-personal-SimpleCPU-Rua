`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:09:32 06/26/2019 
// Design Name: 
// Module Name:    GRF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "SimCtrl.h"
module GRF(
	input	Clk,
	input	Clr,
	input	[31:0]	PC,
	input	[4:0]	Addr1,
	input	[4:0]	Addr2,
	input	[4:0]	WriteAddr,
	input	WriteEnable,
	input	[31:0]	WriteData,
	output	[31:0]	OutData1,
	output	[31:0]	OutData2
    );
	reg	[31:0]	Reg	[31:0];
	assign	OutData1	=	(Addr1 == 0)						?	0:
							(WriteAddr == Addr1 && WriteEnable)	?	WriteData:
																	Reg[Addr1];
	assign	OutData2	=	(Addr2 == 0)						?	0:
							(WriteAddr == Addr2 && WriteEnable)	?	WriteData:
																	Reg[Addr2];
																	//加入了内部转发，省去了W级的转发问题
	////////////////////////////////////////////////////////
	integer	i;
	initial	begin
		for(i=0;i<32;i=i+1)
			Reg[i]=0;
	end
	///////////////////////////////////////////////////////
	always @ (posedge Clk) begin
		if(Clr)begin
			for(i=0;i<32;i=i+1)
				Reg[i]=0;
		end
		else if(WriteEnable) begin
			// $display("@%h: $%d <= %h", PC, WriteAddr,WriteData);
			if(WriteAddr!=0) begin
				Reg[WriteAddr]=WriteData;
			`ifdef GRF_ECHO
			// $display("%d@%h: $%d <= %h", $time, PC, WriteAddr,WriteData);
			$display("%h: $%d <= %h", PC, WriteAddr,WriteData);
			`endif
			end
		
		end
	end
	
endmodule
