`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/02 10:19:05
// Design Name: 
// Module Name: InstrFetchState
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define TextAddr 32'h8000_0000
//`define TextAddr 32'hbfc00000
module InstrFetchState(
    input   Clk,
    input   Clr,
    input   		D_stall_Pass,
    input   [31:0]  D_NewPC_Pass,
    output reg	[31:0]	I_PC,
	output		[31:0]	I_PC_Pass,
    // output reg	[31:0] 	I_Instr,

    output [31:0] 	I_Instr,
    /////////////////new add for SRAM:

	output	[31:0] I_sram_addr,
    input	[31:0] I_sram_data,
	input	I_sram_busy,//与M级的访问出现冲突
    					//让D级停一停
	output  [31:0] I_next_pc_pass
    );

	//////////////////////////////////////////////////////////////////////////////
    assign I_stall_Pass = I_sram_busy;

    reg [31:0] PC_Inter;
	reg [31:0] Instr_r;
    wire[31:0] Instr_Inter;

	assign I_PC_Pass = PC_Inter;
    assign Instr_Inter = I_sram_data;
	assign I_sram_addr = PC_Inter;
    //////////////////////////////////////////////////////////////////////////////

	assign I_Instr = Instr_r;

	assign	I_next_pc_pass = Clr ? `TextAddr:
							 (D_stall_Pass||I_sram_busy)?PC_Inter:D_NewPC_Pass;
    always  @   (posedge Clk)   begin
		if(Clr) begin
			PC_Inter<=`TextAddr;
			I_PC <= 0;
			Instr_r<= 0;
			// Instr_r <= 0;
		end
		else if(D_stall_Pass || I_sram_busy) begin
			PC_Inter<=PC_Inter;
		end
		else begin
			I_PC <= PC_Inter;
			Instr_r<= Instr_Inter;
			PC_Inter<=D_NewPC_Pass;
		end
    end
    ///////////////////////////////////////////////////////////////////////////////
endmodule
