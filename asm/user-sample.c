char getchar();
void putchar(char);
inline void print(const char str[]);
void fibo(int n,unsigned int addr);
__asm__("li $sp,0x80300000");

void __start()
{
    fibo(4096,0x80400000);
    return;
}

 void fibo(int n,unsigned int addr)
{
    register int i=2;
    register int *a = (int*) addr;
    register int t1=2,t2=3,t3;

    a[0]=2;
    a[1]=3;
    while(i<n){
        t3=t1+t2;
        a[i] = t3;
        t1=t2;
        t2=t3;
        i++;
    }
}



void print(const char str[]){
    while(str[0])
    {
        putchar(str[0]);
        str++;
    }
}

// getchar
__asm__(
    ".set noreorder\n\t"
    // ".type    @function\n\t"
    "getchar:\n\t"
    "li $t1,0xBFD003FC\n\t"//status
    "getchar_wait_for_recive_loop:\n\t"
    "lw $t2,0($t1)\n\t"
    "andi $t2,$t2,2\n\t"
    "beqz $t2,getchar_wait_for_recive_loop\n\t"
    "nop\n\t"
    "jr $ra\n\t"
    "lb $v0,-4($t1)\n\t"
);
// putchar
__asm__(
    ".set noreorder\n\t"
    // ".type    @function\n\t"
    "putchar:\n\t"
    "li $t1,0xBFD003FC\n\t"//status
    "putchar_wait_for_idle_loop:\n\t"
    "lw $t2,0($t1)\n\t"
    "andi $t2,$t2,1\n\t"
    "beqz $t2,putchar_wait_for_idle_loop\n\t"
    "nop\n\t"
    "jr $ra\n\t"
    "sb $a0,-4($t1)\n\t"
);
