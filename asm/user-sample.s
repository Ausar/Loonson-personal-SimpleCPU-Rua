.set noreorder
# .set noat
.globl __start
.section text
.text
__start:

	li $s1,0 #i
	li $s2,0x31110
	li $s3,0#sum
	out_loop:
		slti $v0,$s1,0 # 硬件实现的计数指令
		slt $t1,$s2,$s1
		addiu $s1,$s1,1 # i++
		beqz $t1,out_loop
		# ble $s1,$s2,out_loop
		add $s3,$s3,$v0 #sum+=ans
	lui $t1,0x8040
	jr $ra
	sw $s3,0($t1)


	# li $s1,0 #i
	# li $s2,0x31111
	# li $s3,0#sum
	# out_loop:
	# 	move $a0,$s1
	# 	li $v0,0
	# 	cnt_loop:
	# 		andi $t1,$a0,1
	# 		srl $a0,$a0,1
	# 		bnez $a0,cnt_loop
	# 		add $v0,$v0,$t1
	# 	cnt_end:
	# 	addiu $s1,$s1,1 # i++
	# 	ble $s1,$s2,out_loop
	# 	add $s3,$s3,$v0 #sum+=ans
	# lui $t1,0x8040
	# jr $ra
	# sw $s3,0($t1)